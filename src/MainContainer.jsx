import styled from "styled-components";
import Dashboard from "./Dashboard";
import NewAssessment from "./NewAssessment";
import Reports from "./Reports";
import NeedHelp from "./NeedHelp";

const Container = styled.div`
  padding: 20px;
`;

export default function MainContainer(props) {
  return (
    <Container>
      {props.selected === 0 ? (
        <Dashboard />
      ) : props.selected === 1 ? (
        <NewAssessment />
      ) : props.selected === 2 ? (
        <Reports />
      ) : (
        <NeedHelp />
      )}
    </Container>
  );
}
