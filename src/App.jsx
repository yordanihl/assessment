import { useState } from "react";
import styled from "styled-components";
import GlobalStyle from "./globalStyle";
import Navigation from "./Navigation";
import MainContainer from "./MainContainer";

const Grid = styled.div`
  display: grid;
  grid-template-columns: 200px auto;
  height: 100vh;
`;

export default function App() {
  const [selected, setSelected] = useState(0);

  return (
    <Grid>
      <GlobalStyle />
      <Navigation setSelected={setSelected} />
      <MainContainer selected={selected} />
    </Grid>
  );
}
