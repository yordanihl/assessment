import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`
	*{
		box-sizing: border-box;
	}
	body{
		margin: 0px;
		padding: 0px;
	}
`;
