import styled from "styled-components";

const Menu = styled.div`
  padding: 10px;
  background-color: #ffffbb;
  height: 100%;
`;

const Button = styled.div`
  margin: 10px 0px;
  padding: 10px 20px;
  background-color: #ffbbbb;
  cursor: pointer;
`;

const btns = [
  { id: 0, name: "Dashboard" },
  { id: 1, name: "New Assessment" },
  { id: 2, name: "Reports" },
  { id: 3, name: "Need Help?" },
];

export default function Navigation(props) {
  return (
    <Menu>
      {btns.map((btn) => (
        <Button key={btn.id} onClick={props.setSelected.bind(null, btn.id)}>
          {btn.name}
        </Button>
      ))}
    </Menu>
  );
}
